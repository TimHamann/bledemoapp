package com.example.tim.bledemoapp3.thiemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.tim.bledemoapp3.R;
import com.stabilo.digipenkit.CalibrationData;
import com.stabilo.digipenkit.DigipenCalibration;
import com.stabilo.digipenkit.StreamData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by thiemodaubner on 27.02.18.
 * Cool
 */

public class DigipenCalibrator extends Activity implements ConnectionHolder.DigipenConnect, ConnectionHolder.PencoderApplication {

    final static String CALIBRATION_FILE = "calibration.csv";

    ConnectionHolder connectionHolder;

    ProgressBar progressBar1;
    ProgressBar progressBar2;
    ProgressBar progressBar3;

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibrate_digipen);

        progressBar1 = findViewById(R.id.pb_calib1);
        progressBar2 = findViewById(R.id.pb_calib2);
        progressBar3 = findViewById(R.id.pb_calib3);

        imageView = findViewById(R.id.iv_imageHolder);

        connectionHolder = ((ConnectionHolder) this.getApplication());

    }

    @Override
    protected void onResume() {

        super.onResume();

        connectionHolder.setInterface(this);
        connectionHolder.startDigipenCalibration();
    }

    @Override
    protected void onDestroy() {

        connectionHolder.stopDigipenCalibration();
        super.onDestroy();
    }

    @Override
    public void on_digipenIsConnected(boolean connected) {

    }

    @Override
    public void on_digipenBatteryHasChanged(int battery) {

    }

    @Override
    public void on_digipenNameHasChanged(String name) {

    }

    @Override
    public void on_digipenFirmwareVersionHasChanged(String firmware) {

    }

    @Override
    public void on_digipenSerialHasChanged(String serial) {

    }

    @Override
    public void on_digipenLastCalibrationDateChanged(Date date) {

    }

    @Override
    public void on_digipenCalibration(DigipenCalibration.CalibrationState calibrationState, Double stepProgress, DigipenCalibration.CalibrationAction calibrationAction) {

        stepProgress = stepProgress * 100;

        switch (calibrationState) {

            case STEP_1:

                Tools.setProgressBarValue(true, stepProgress.intValue(), progressBar1, this);
                Tools.setProgressBarValue(true, 0, progressBar2, this);
                Tools.setProgressBarValue(true, 0, progressBar3, this);
                Tools.setImageResources(R.mipmap.keep_calm, imageView, this);
                break;

            case STEP_2:

                Tools.setProgressBarValue(true, stepProgress.intValue(), progressBar2, this);
                Tools.setProgressBarValue(true, 0, progressBar3, this);
                Tools.setImageResources(R.mipmap.move_it, imageView, this);
                break;

            case STEP_3:

                Tools.setProgressBarValue(true, stepProgress.intValue(), progressBar3, this);
                Tools.setImageResources(R.mipmap.nearly_done, imageView, this);
                break;
        }

        if (calibrationAction == DigipenCalibration.CalibrationAction.WAIT_FOR_USER_ACKNOWLEDGE) {

            Runnable runner = new Runnable() {
                @Override
                public void run() {
                    connectionHolder.startDigipenStream();
                }
            };
            Tools.startDelayed(runner, 500, this);

        } else if (calibrationState == DigipenCalibration.CalibrationState.FINISHED) {

            Tools.setImageResources(R.mipmap.finish, imageView, this);
            Runnable runner = new Runnable() {
                @Override
                public void run() {
                    Intent startActivity = new Intent();
                    setResult(RESULT_OK, startActivity);
                    finish();
                }
            };
            Tools.startDelayed(runner, 1000, this);

        }
    }

    @Override
    public void on_digipenDataStream(StreamData streamData) {

    }

    @Override
    public void on_digipenOffPaper() {

    }

    @Override
    public void on_digipenIsNotStreaming() {

    }

    @Override
    public void on_initError(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void on_foundPairedDevice(String device) {

    }

    static void writeCalibrationFile(File folder, SessionData sessionData) {

        try {

            File path = new File(folder, DigipenCalibrator.CALIBRATION_FILE);
            FileOutputStream stream = new FileOutputStream(path);

            if (path.exists() && !path.isDirectory()) {

                CalibrationData calibrationData = sessionData.calibrationData;

                stream.write(("// pen_nr:" + sessionData.digipenSerial + "\n").getBytes());
                stream.write(("smx:" + calibrationData.smx + "\n").getBytes());
                stream.write(("smy:" + calibrationData.smy + "\n").getBytes());
                stream.write(("smz:" + calibrationData.smz + "\n").getBytes());
                stream.write(("sax:" + calibrationData.sax + "\n").getBytes());
                stream.write(("say:" + calibrationData.say + "\n").getBytes());
                stream.write(("saz:" + calibrationData.saz + "\n").getBytes());
                stream.write(("sgx:" + calibrationData.sgx + "\n").getBytes());
                stream.write(("sgy:" + calibrationData.sgy + "\n").getBytes());
                stream.write(("sgz:" + calibrationData.sgz + "\n").getBytes());
                stream.write(("scm:" + calibrationData.scm + "\n").getBytes());
                stream.write(("bmx:" + calibrationData.bmx + "\n").getBytes());
                stream.write(("bmy:" + calibrationData.bmy + "\n").getBytes());
                stream.write(("bmz:" + calibrationData.bmz + "\n").getBytes());
                stream.write(("bax:" + calibrationData.bax + "\n").getBytes());
                stream.write(("bay:" + calibrationData.bay + "\n").getBytes());
                stream.write(("baz:" + calibrationData.baz + "\n").getBytes());
                stream.write(("bgx:" + calibrationData.bgx + "\n").getBytes());
                stream.write(("bgy:" + calibrationData.bgy + "\n").getBytes());
                stream.write(("bgz:" + calibrationData.bgz + "\n").getBytes());
                stream.write(("gf:" + calibrationData.gf + "\n").getBytes());
                stream.write(("q_p:" + calibrationData.q_p + "\n").getBytes());
                stream.write(("q_v:" + calibrationData.q_v + "\n").getBytes());
                stream.write(("q_a:" + calibrationData.q_a + "\n").getBytes());
                stream.write(("q_g:" + calibrationData.q_g + "\n").getBytes());
                stream.write(("q_m:" + calibrationData.q_m + "\n").getBytes());
                stream.write(("r_a:" + calibrationData.r_a + "\n").getBytes());
                stream.write(("r_g:" + calibrationData.r_g + "\n").getBytes());
                stream.write(("r_m:" + calibrationData.r_m + "\n").getBytes());
                stream.write(("r_v:" + calibrationData.r_v + "\n").getBytes());
                stream.write(("r_p:" + calibrationData.r_p + "\n").getBytes());
                stream.write(("beta_1:" + calibrationData.beta_1 + "\n").getBytes());
                stream.write(("beta_2:" + calibrationData.beta_2 + "\n").getBytes());
                stream.write(("date:" + calibrationData.lastUpdateDate + "\n").getBytes());

                stream.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
