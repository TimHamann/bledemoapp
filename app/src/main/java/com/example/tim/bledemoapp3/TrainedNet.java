package com.example.tim.bledemoapp3;


import android.app.Activity;

import org.datavec.api.records.reader.SequenceRecordReader;
import org.datavec.api.records.reader.impl.csv.CSVSequenceRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.SequenceRecordReaderDataSetIterator;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
import org.nd4j.linalg.dataset.api.preprocessor.serializer.NormalizerSerializer;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/**
 * Encapsulates the methods concerning the trained neural network
 */
public class TrainedNet {
    private static int numClasses = 13;
    private static int numColumns = 15;

    private File dummyLabel; //only needed for the record reader
    private SequenceRecordReader reader;
    private SequenceRecordReader labelReader;

    static MultiLayerNetwork model;
    static NormalizerStandardize normalizer;

    /**
     * Constructor
     * @param folder the app's session folder
     * @param activity the current activity
     */
    public TrainedNet(String folder, Activity activity) {
        //initialize model and normalizer
        InputStream inputStream = activity.getResources().openRawResource(R.raw.lstm100tim1ep);
        try {
            this.model = ModelSerializer.restoreMultiLayerNetwork(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        inputStream = activity.getResources().openRawResource(R.raw.normalizer100tim);
        NormalizerSerializer serializer = NormalizerSerializer.getDefault();
        try {
            this.normalizer = serializer.restore(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }


        dummyLabel = new File(folder + "/labelDummy.csv");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(dummyLabel.getPath(), "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println("0");
        writer.close();

        labelReader = new CSVSequenceRecordReader(0, ";");
        reader = new CSVSequenceRecordReader(1, ";");
        try {
            labelReader.initialize(new FileSplit(dummyLabel));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * Uses the trained net to make predictions
     * @param file the feature file to use as input
     * @param numSplits the amount of splits to make in the file (V has one split, N has two). -1 initiates auto mode, trying different amounts of splits
     * @return a list of BLEs and their corresponding probability, according to the net
     * @throws IOException
     * @throws InterruptedException
     */
    public List<BleWithProb> useTrainedNet(File file, int numSplits) throws IOException, InterruptedException {


        reader.initialize(new FileSplit(file));
        DataSetIterator iter = new SequenceRecordReaderDataSetIterator(reader, labelReader,
                1, numClasses, false, SequenceRecordReaderDataSetIterator.AlignmentMode.ALIGN_END);

        DataSet dataSet = iter.next();

        INDArray features = dataSet.getFeatures();
        if (numSplits != -1) {
            List<BleWithProb> results = doRecursiveClassification(features, numSplits);
            return results;
        } else {
            int sampleLength = features.getColumn(0).toDoubleVector().length;
            List<BleWithProb> results0 = null;
            List<BleWithProb> results1 = null;
            List<BleWithProb> results2 = null;
            List<BleWithProb> results3 = null;
            double avg0 = 0;
            double avg1 = 0;
            double avg2 = 0;
            double avg3 = 0;

            if (sampleLength <= 100) {
                results0 = doRecursiveClassification(features, 0);
                avg0 = results0.get(0).getProbability();

            }
            if (sampleLength >= 25 && sampleLength <= 160){
                results1 = doRecursiveClassification(features, 1);
                avg1 = sumUpCertainties(results1) / 2;
            }
            if (sampleLength >= 40 && sampleLength <= 300){
                results2 = doRecursiveClassification(features, 2);
                avg2 = sumUpCertainties(results2) / 3;
            }
            if(sampleLength >= 60){
                results3 = doRecursiveClassification(features, 3);
                avg3 = sumUpCertainties(results3) / 4;
            }


            System.out.println("Sample Length: " + sampleLength);
            System.out.println("avg0: " + avg0 + " " + resultBlesAsString(results0));
            System.out.println("avg1: " + avg1 + " " + resultBlesAsString(results1));
            System.out.println("avg2: " + avg2 + " " + resultBlesAsString(results2));
            System.out.println("avg3: " + avg3 + " " + resultBlesAsString(results3));
            int bestResultsNum = getHighest(avg0, avg1, avg2, avg3);

            System.out.println("Best one: avg" + bestResultsNum);

            if (bestResultsNum == 0) {
                return results0;
            } else if (bestResultsNum == 1) {
                return results1;
            } else if (bestResultsNum == 2) {
                return results2;
            } else if (bestResultsNum == 3) {
                return results3;
            }


        }

        return null;
    }

    /**
     * Returns the given BLE list as a readable string
     * @param results the list to convert to a readable string
     * @return
     */
    private String resultBlesAsString(List<BleWithProb> results) {
        if(results == null){
            return "[]";
        }

        Iterator<BleWithProb> iter = results.iterator();
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        while(iter.hasNext()){
            sb.append(iter.next().ble);
            if(iter.hasNext()){
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Computes the index of the highest given number
     * @param avgs the numbers to choose the highest (index) from
     * @return the index of the highest number
     */
    private int getHighest(double... avgs) {
        double bestAvg = 0;
        int bestI = -1;
        for (int i = 0; i < avgs.length; ++i) {
            if (avgs[i] > bestAvg) {
                bestAvg = avgs[i];
                bestI = i;
            }
        }
        return bestI;
    }

    /**
     * Performs the classification recursively (subtracting 1 from numSplits every call)
     * @param features the features to classify
     * @param numSplits the amount of splits to perform
     * @return a list with BLEs and their probability
     */
    private List<BleWithProb> doRecursiveClassification(INDArray features, int numSplits) {
        List<BleWithProb> result = new ArrayList<>(1);
        int sampleLength = features.getColumn(0).toDoubleVector().length;
        System.out.println("num separations: " + numSplits);

        if (numSplits == 0) {
            result.add(splitFeaturesAndGetCertainty(features, sampleLength)); //"split" at end of sample
            return result;
        }


        List<Integer> firstSep = chooseSeparations(numSplits, sampleLength); //gibt nur den Ort der ERSTEN Separation zurück
        System.out.println("Proposed separations: " + firstSep.toString() + ", sample length: " + sampleLength);


        int bestSep = -1;
        BleWithProb bestBLE = new BleWithProb(0, -1);
        for (int i = 0; i < firstSep.size(); ++i) {
            System.out.println("--" + i + "--");

            BleWithProb currentBLESplit = splitFeaturesAndGetCertainty(features, firstSep.get(i));

            if (currentBLESplit.getProbability() > bestBLE.getProbability()) {
                bestBLE = currentBLESplit;
                bestSep = i;
            }
        }

        INDArray newInd = cutOffClassifiedBLE(features, firstSep.get(bestSep));
        result.add(bestBLE);
        System.out.println("best separation: " + bestSep + "(" + firstSep.get(bestSep) + "/" + sampleLength + ")");
        result.addAll(doRecursiveClassification(newInd, numSplits - 1));
        return result;
    }

    /**
     * Cuts of the first, already split and classified, part of the features
     * @param features the features to perform the cut on
     * @param separation the sample to cut off at
     * @return the resulting smaller feature matrix
     */
    private INDArray cutOffClassifiedBLE(INDArray features, int separation) {
        INDArray rows = features.getRow(0);
        int size = (rows.columns() - separation);
        INDArray newInd2 = Nd4j.zeros(1, numColumns, size);
        for (int i = 0; i < size; ++i) {
            newInd2.getRow(0).putColumn(i, rows.getColumn(i + (separation)));
        }
        return newInd2;
    }

    /**
     * Only looks at the matrix until splitPosition and returns the neural net's output
     * @param features the features to classify
     * @param splitPosition the position to split at
     * @return the resulting BLEWithProb
     */
    private BleWithProb splitFeaturesAndGetCertainty(INDArray features, int splitPosition) {
//		int[] shape = features.shape(); //1,15,zb68
//		INDArray cols = features.getColumn(0); //erste SPALTE
        INDArray rows = features.getRow(0); //ganze Matrix (in 2 arrays, nicht mehr in 3)
//		int colNum = featureMatrix.columns(); //exception
//		int rowNum = featureMatrix.rows(); //exception
//		INDArray rowsrow = rows.getRow(0); //auch erste SPALTE
//		INDArray rowscol = rows.getColumn(0); //ERSTE ZEILE JAAAAAAA


        INDArray ind = Nd4j.zeros(1, numColumns, splitPosition);
        for (int i = 0; i < splitPosition; ++i) {
            ind.getRow(0).putColumn(i, rows.getColumn(i));
        }

        normalizer.transform(ind);

        return interpretLSTMOutput(model.output(ind));


    }

    /**
     * Interprets the neural net's output INDArray
     * @param output the INDArray put out by the net
     * @return the BLE with the highest probability
     */
    public static BleWithProb interpretLSTMOutput(INDArray output) {

        int timeSeriesLength = output.size(2);
        INDArray probabilitiesAtLastSample = output.get(NDArrayIndex.point(0), NDArrayIndex.all(),
                NDArrayIndex.point(timeSeriesLength - 1));

        double[] probabilities = new double[numClasses];

        // System.out.println("\n\n-------------------------------");
        // System.out.println("\n\nProbabilities at last time step:");
        for (int i = 0; i < probabilitiesAtLastSample.length(); ++i) {
            // System.out.println("p(" + i + "): " +
            // probabilitiesAtLastSample.getDouble(i));
            probabilities[i] = probabilitiesAtLastSample.getDouble(i);
        }

        double largestProb = probabilities[0];
        int highestIndex = 0;
        for (int i = 1; i < probabilities.length; i++) {
            if (probabilities[i] > largestProb) {
                largestProb = probabilities[i];
                highestIndex = i;
            }
        }
        System.out.println("---------------");
        System.out.println(
                "Classified as " + getLabelNames().get(highestIndex) + " with " + largestProb + " certainty.");

        return (new BleWithProb(highestIndex, largestProb));
    }


    /**
     * Chooses samples to perform the FIRST of x splits at
     * @param numSplits the amount of splits to perform
     * @param length the length of the current sample
     * @return possibilities of doing the FIRST of x splits
     */
    private static List<Integer> chooseSeparations(int numSplits, int length) {
        if (numSplits == 1) {//first half
            List<Integer> seps = new ArrayList<Integer>(10);
            int half = Math.round(length / 2);
            seps.add(half);
            for (int i = 1; i < 6; i++) {
                int minusOne = half - i;
                int plusOne = half + i;
                if (minusOne > Math.round(length / 4)) {
                    seps.add(minusOne);
                }
                if (plusOne < Math.round(3 * length / 4)) {
                    seps.add(plusOne);
                }
            }
            Collections.sort(seps);
            return seps;
        } else if (numSplits == 2) { //first third
            List<Integer> seps = new ArrayList<Integer>(10);
            int third = Math.round(length / 3);
            seps.add(third);
            for (int i = 1; i < 6; ++i) {
                int minusOne = third - i;
                int plusOne = third + i;
                if (minusOne > Math.round(length / 6)) {
                    seps.add(minusOne);
                }
                if (plusOne < Math.round(3 * length / 6)) {
                    seps.add(plusOne);
                }
            }
            Collections.sort(seps);
            return seps;
        } else if (numSplits == 3) { // first fourth
            List<Integer> seps = new ArrayList<Integer>(10);
            int half = Math.round(length / 4);
            seps.add(half);
            for (int i = 1; i < 6; ++i) {
                int minusOne = half - i;
                int plusOne = half + i;
                if (minusOne > Math.round(length / 8)) {
                    seps.add(minusOne);
                }
                if (plusOne < Math.round(3 * length / 8)) {
                    seps.add(plusOne);
                }
            }
            Collections.sort(seps);
            return seps;
        }
        return null;
    }

    /**
     * Sums up the given certainties
     * @param certainties the BleWithProb to sum the certainties up from
     * @return the summed up certainties
     */
    private static double sumUpCertainties(List<BleWithProb> certainties) {
        Iterator<BleWithProb> it = certainties.iterator();
        double sum = 0;
        while (it.hasNext()) {
            sum += it.next().getProbability();
        }
        return sum;
    }

    /**
     * Returns an ordered list of label names
     * @return a list of label names, ordered
     */
    public static List<String> getLabelNames() {
        List<String> labelNames = new ArrayList<>();
        labelNames.add("CIRC_CCW_TR");
        labelNames.add("DIAG_DOWN_FLTR");
        labelNames.add("DIAG_DOWN_FRTL");
        labelNames.add("DIAG_UP_FLTR");
        labelNames.add("DOT");
        labelNames.add("HOOK_BOTTOM_LEFT_DOWN");
        labelNames.add("HORIZ_FLTR");
        labelNames.add("HORIZ_FRTL");
        labelNames.add("SEMCIRC_HORIZ_DOWN_CCW");
        labelNames.add("SEMCIRC_VERT_LEFT_CCW");
        labelNames.add("SEMCIRC_VERT_RIGHT_CW");
        labelNames.add("VERT_DOWN");
        labelNames.add("VERT_UP");

        return labelNames;
    }


    /**
     * Class encapsulating a BLE and the corresponding probability.
     */
    public static class BleWithProb {
        private BLE ble;
        private double probability;
        private int index;

        /**
         * Constructor
         * @param index the BLE's index
         * @param probability the BLE's probability
         */
        public BleWithProb(int index, double probability) {
            this.ble = BLE.valueOf(getLabelNames().get(index));
            this.probability = probability;
            this.index = index;
        }

        /**
         * Get the BLE
         * @return the BLE
         */
        public BLE getBle() {
            return ble;
        }

        /**
         * Get the probability
         * @return the probability
         */
        public double getProbability() {
            return probability;
        }

        /**
         * Get the index
         * @return the BLE's index
         */
        public int getIndex() {
            return index;
        }
    }
}
