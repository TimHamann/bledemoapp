package com.example.tim.bledemoapp3.thiemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.tim.bledemoapp3.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by thiemodaubner on 21.02.18.
 * Cool
 */

public class Tools {

    public static void setText(final boolean connected, final String text, final Button button, Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connected) {
                    button.setText(text);
                } else {
                    button.setText(text);
                }
            }
        });
    }

    static void setText(final boolean connected, final String text, final EditText editText, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connected) {
                    editText.setText(text);
                } else {
                    editText.setText("");
                }
            }
        });
    }

    public static void setText(final boolean connected, final String text, final TextView textView, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connected) {
                    textView.setText(text);
                } else {
                    textView.setText("");
                }
            }
        });
    }


    static void setTextColor(final TextView textView, final int color, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setTextColor(color);
            }
        });
    }


    static void setImageResources(final int resource, final ImageView imageView, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageView.setImageResource(resource);
            }
        });
    }

    static void setImageResources(final File resource, final ImageView imageView, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (resource.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(resource.getAbsolutePath());
                    imageView.setImageBitmap(myBitmap);
                }
            }
        });
    }

    static void setImageResources(final int resource, final TextView textView, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setCompoundDrawablesWithIntrinsicBounds(resource, 0, 0, 0);
            }
        });
    }

    static void setImageResources(final int resource, final ImageButton imageButton, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageButton.setImageResource(resource);
            }
        });
    }


    public static void setButtonState(final Button button, final ButtonState state, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (state == ButtonState.enabled) {
                    button.setEnabled(true);
                }

                if (state == ButtonState.disabled) {
                    button.setEnabled(false);
                }
            }
        });
    }


    static void setBackground(final boolean connected, final int color, final TextView textView, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connected) {
                    textView.setBackgroundResource(color);
                } else {
                    textView.setBackgroundResource(R.color.colorLightBackground);
                }
            }
        });
    }

    static void setBackground(final boolean connected, final int ressource, final ImageButton button, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connected) {
                    button.setBackground(activity.getDrawable(ressource));
                }
            }
        });
    }

//    static void setBackground(final boolean connected, final int color, final ProgressBar progressBar, final StartActivity activity) {
//
//        activity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (connected) {
//                    progressBar.getProgressDrawable().setColorFilter(activity.getColor(color), PorterDuff.Mode.MULTIPLY);
//                }
//            }
//        });
//    }


    static void setVisibility(final int visibility, final ProgressBar progressBar, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(visibility);
            }
        });
    }

    static void setVisibility(final boolean stream, final int visibility, final ImageView imageView, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (stream) {
                    imageView.setVisibility(visibility);
                } else {
                    imageView.setVisibility(View.INVISIBLE);
                }

            }
        });
    }

    static void setVisibility(final boolean stream, final int visibility, final ImageButton button, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (stream) {
                    button.setVisibility(visibility);
                } else {
                    button.setVisibility(View.INVISIBLE);
                }

            }
        });
    }

    static void setVisibility(final boolean stream, final int visibility, final TextView textView, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (stream) {
                    textView.setVisibility(visibility);
                } else {
                    textView.setVisibility(View.INVISIBLE);
                }
            }
        });
    }


    public static void setProgressBarValue(final boolean connected, final int progress, final ProgressBar progressBar, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connected) {
                    progressBar.setProgress(progress);
                } else {
                    progressBar.setProgress(0);
                }

            }
        });
    }

    static String getPreference(Context context, String preferenceKey) {

        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);

        if (sharedPreferences.contains(preferenceKey)) {

            return sharedPreferences.getString(preferenceKey, "");
        }

        return null;
    }

    static void setPreference(Context context, String serializedObjectKey, String string) {

        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();

        sharedPreferencesEditor.putString(serializedObjectKey, string);
        sharedPreferencesEditor.apply();
    }


    static void startDelayed(final Runnable runnable, final int delay, Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Handler h = new Handler();
                h.postDelayed(runnable, delay);
            }
        });
    }


    public static long getDateDifference(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    static String getRightDateFormat(Date date, String format) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.GERMAN);
        return dateFormat.format(date);
    }


    static JSONObject loadJsonFile(File folder, String filename) {

        StringBuilder text = new StringBuilder();
        BufferedReader br = null;
        JSONObject jsonObject = null;

        folder.mkdirs();

        File file = new File(folder, filename);

        try {
            br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            // do exception handling
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                jsonObject = new JSONObject(text.toString());
            } catch (Exception e) {
                // do exception handling
            }
        }

        return jsonObject;
    }


    static void showDialog(final String title, final String message, final String yes, final Runnable runnableYes, final String no, final Runnable runnableNo, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

                // set title
                alertDialogBuilder.setTitle(title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (runnableYes == null) {
                                    dialog.cancel();
                                } else {
                                    Handler h = new Handler();
                                    h.post(runnableYes);
                                }
                            }
                        })
                        .setNegativeButton(no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                if (runnableNo == null) {
                                    activity.finish();
                                } else {
                                    Handler h = new Handler();
                                    h.post(runnableNo);
                                }
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }
}
