package com.example.tim.bledemoapp3;

/**
 * Enum listing all possible Basic Letter Elements (BLEs).
 * @author tim
 *
 */
public enum BLE {

    CIRC_CCW_TR,
    DIAG_DOWN_FLTR,
    DIAG_DOWN_FRTL,
    DIAG_UP_FLTR,
    DOT,
    HOOK_BOTTOM_LEFT_DOWN,
    HORIZ_FLTR,
    HORIZ_FRTL,
    SEMCIRC_HORIZ_DOWN_CCW,
    SEMCIRC_VERT_LEFT_CCW,
    SEMCIRC_VERT_RIGHT_CW,
    VERT_DOWN,
    VERT_UP,




    //UNUSED, but necessary for preprocessing (since they have been recorded originally)
    DIAG_UP_FRTL,

    CIRC_CW_TR, //CIRCLE CLOCKWISE TOP RIGHT (top right is starting position)
    CIRC_CW_BL,
    CIRC_CCW_BL,

    SEMCIRC_VERT_RIGHT_CCW,
    SEMCIRC_VERT_LEFT_CW,

    SEMCIRC_HORIZ_UP_CW, //SEMI CIRCLE HORIZONTAL (line through end points is horizontal) UP (half circle above the line) CW (clockwise)
    SEMCIRC_HORIZ_UP_CCW,
    SEMCIRC_HORIZ_DOWN_CW,

    HOOK_TOP_LEFT_UP, //HOOK TOP (hook is on top) LEFT (looking leftward) UP (direction of writing)
    HOOK_TOP_LEFT_DOWN,
    HOOK_TOP_RIGHT_UP,
    HOOK_TOP_RIGHT_DOWN,

    HOOK_BOTTOM_LEFT_UP,
    HOOK_BOTTOM_RIGHT_UP,
    HOOK_BOTTOM_RIGHT_DOWN;
}
