package com.example.tim.bledemoapp3.thiemo;

import com.stabilo.digipenkit.CalibrationData;

import java.io.File;
import java.util.Date;

/**
 * Created by thiemodaubner on 01.03.18.
 * Cool
 */

public class SessionData {

    public String appVersion;
    public Date date;
    String birth;
    int birth_day;
    int birth_month;
    int birth_year;
    String hand;
    String gender;
    String userID;
    boolean computedUserID = true;
    public File folder;
    public String digipenName;
    public String digipenSerial;
    public String digipenFirmwareVersion;
    public int digipenBattery;
    public Date calibrationDate;
    CalibrationData calibrationData;
}