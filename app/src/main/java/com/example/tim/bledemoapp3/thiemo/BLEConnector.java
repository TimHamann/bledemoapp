package com.example.tim.bledemoapp3.thiemo;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.os.Handler;
import android.os.ParcelUuid;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * <p>In this class we build a connection to the digipen. When a device is found<p/>
 * <p>a new button will be added under the scan button. On click on that button, the connection to<p/>
 * <p>this device will be established<p/>
 */
public class BLEConnector {

    private final int ROUNDS_TO_LOOSE_DEVICE = 50;

    private boolean SEARCH_WITH_FILTERS;
    private final UUID SERVICE_HID = UUID.fromString("7e0bc6be-8271-4f5a-a126-c24220e6250c");
    private long SCAN_PERIOD = 10*60*1000;

    /**
     * In this Hashtable the found devices will be hold
     */
    private Hashtable<BluetoothDevice, Integer> devices = new Hashtable<>();
    private HashSet<BLEDevice> bleDevices = new HashSet<>();

    private BLEResult bleResult;

    private BluetoothLeScanner bluetoothLeScanner;
    private ScanSettings scanSettings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
    private Handler handler;

    public interface BLEResult {
        void onBLEScanResult(HashSet<BLEDevice> bleDevices);
    }

    BLEConnector(BLEResult bleResult, BluetoothAdapter bluetoothAdapter) {


        this.bleResult = bleResult;
        this.handler = new Handler();
        // Bluetoothmanager initialisieren
        this.bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
    }

    void setSearchFilterOn(boolean value) {
        this.SEARCH_WITH_FILTERS = value;
    }

    void setScanPeriod(long SCAN_PERIOD) {
        this.SCAN_PERIOD = SCAN_PERIOD;
    }

    /**
     * <p>This method starts the scan for ble devices. If a device is found, the mScanCallback method<p/>
     * <p>will be called. This method expects a flag that enables or disables the connection<p/>
     *
     * @throws Exception if something went wrong
     */
    void scanForDevices(boolean enable) throws Exception {

        if (enable) {

            devices.clear();
            bleDevices.clear();
            List<ScanFilter> filters;

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    bluetoothLeScanner.stopScan(mScanCallback);
                }
            }, SCAN_PERIOD);


            // check if the filter is turned on. This allows to only show digipens
            if (SEARCH_WITH_FILTERS) {

                filters = Collections.singletonList(new ScanFilter.Builder().setServiceUuid(new ParcelUuid(SERVICE_HID)).build());

            } else {

                filters = new ArrayList<>();

            }

            bluetoothLeScanner.startScan(filters, scanSettings, mScanCallback);

        } else {

            bluetoothLeScanner.stopScan(mScanCallback);
        }
    }

    /**
     * <p>The callback from the scanLEDevice method receives the devices in every positive scan result. This might be<p/>
     * <p>a lot devices because in every callback all found devices will be shown up. because of that we<p/>
     * <p>need a table in that we write only the new or changed devices. After that this devices show up<p/>
     * <p>in form of a button. By clicking on that button, a connection to this device will be enabled.<p/>
     */
    private ScanCallback mScanCallback = new ScanCallback() {

        @Override
        public void onScanResult(int callbackType, ScanResult result) {

            // ---- Get the Device of the result
            final BluetoothDevice device = result.getDevice();

            // ---- Try to get the name of the device...
            String deviceName;
            try {
                deviceName = result.getScanRecord().getDeviceName();
            } catch (NullPointerException e) {
                deviceName = null;
            }

            // ---- Get the RSSI of the device
            int rssi = result.getRssi();

            // Check if a device does not respond any more
            checkForLostDevices(device);

            // ---- Write the device in a table if it is not in there yet.
            if (!devices.containsKey(device)) {

                devices.put(device, ROUNDS_TO_LOOSE_DEVICE);

                // Build a bleDevice
                BLEDevice bleDevice = new BLEDevice(device, deviceName, rssi);

                bleDevices.add(bleDevice);
                sendDeviceData();
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
        }

        @Override
        public void onScanFailed(int errorCode) {
        }
    };

    private void checkForLostDevices(BluetoothDevice device) {

        Set<BluetoothDevice> keys = devices.keySet();

        for (BluetoothDevice key : keys) {

            int count = devices.get(key);
            int newCount = count - 1;

            if (Objects.equals(device, key)) {

                devices.put(key, ROUNDS_TO_LOOSE_DEVICE);

            } else {

                if (newCount == 0) {

                    devices.remove(key);

                    deleteBleDevice(key);

                    sendDeviceData();

                    break;

                } else {

                    devices.put(key, newCount);
                }
            }
        }
    }

    private void deleteBleDevice(BluetoothDevice name) {

        for (BLEDevice bleDevice : bleDevices) {

            if (Objects.equals(bleDevice.address, name)) {

                bleDevices.remove(bleDevice);
                break;
            }
        }
    }

    private void sendDeviceData() {
        bleResult.onBLEScanResult(bleDevices);
    }

    /**
     * <p>This class holds the data from the received bluetooth device</p>
     */
    public class BLEDevice implements Serializable {
        BluetoothDevice address;
        public String name;
        int rssi;

        BLEDevice(BluetoothDevice address, String name, int rssi) {
            this.address = address;
            this.name = name;
            this.rssi = rssi;
        }
    }
}
