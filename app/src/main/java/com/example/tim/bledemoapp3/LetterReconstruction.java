package com.example.tim.bledemoapp3;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides means to reconstruct sequences of BLEs to complete letters.
 *
 * @author tim
 */
public class LetterReconstruction {

    /**
     * Reconstructs complete letters form the given sequence
     *
     * @param labels the labels to do the reconstruction with
     * @return the reconstructed letter or "?"
     */
    public static String reconstruct(int... labels) {

        List<BLE> sequence = new ArrayList<BLE>(4);
        for (int i = 0; i < labels.length; ++i) {
            int label = labels[i];
            if (label != -1) {
                sequence.add(getBLEName(label));
            }
        }

        return reconstruct(sequence);
    }

    private static boolean isUpward(BLE ble) {
        return ble.equals(BLE.VERT_UP) || ble.equals(BLE.DIAG_UP_FLTR);
    }

    private static boolean isDownward(BLE ble) {
        return ble.equals(BLE.VERT_DOWN) || ble.equals(BLE.DIAG_DOWN_FLTR) || ble.equals(BLE.DIAG_DOWN_FRTL)
                || ble.equals(BLE.HOOK_BOTTOM_LEFT_DOWN);
    }

    private static boolean isRightward(BLE ble) {
        return ble.equals(BLE.HORIZ_FLTR) || ble.equals(BLE.SEMCIRC_HORIZ_DOWN_CCW) || ble.equals(BLE.DIAG_DOWN_FLTR) || ble.equals(BLE.DIAG_UP_FLTR) || ble.equals(BLE.DOT);// ||
        // ble.equals(BLE.DOT);
    }

    private static boolean isCW(BLE ble) {
        return ble.equals(BLE.SEMCIRC_VERT_RIGHT_CW) || ble.equals(BLE.HOOK_BOTTOM_LEFT_DOWN);// || ble.equals(BLE.DOT)
        // ;
    }

    private static boolean isLeftward(BLE ble) {// TODO HORIZ FRTL evtl ganz weglassen?
        return ble.equals(BLE.DIAG_DOWN_FRTL) || ble.equals(BLE.HORIZ_FRTL);
    }

    private static String reconstruct(List<BLE> sequence) {
        System.out.println("PROCESSING SEQUENCE:");
        System.out.println(sequence.toString());
        int size = sequence.size();

        BLE ble1 = sequence.get(0);
        BLE ble2 = null;
        BLE ble3 = null;
        BLE ble4 = null;

        if (size > 1) {
            ble2 = sequence.get(1);
        }
        if (size > 2) {
            ble3 = sequence.get(2);
        }
        if (size > 3) {
            ble4 = sequence.get(3);
        }


        if (size == 1) { // C, J1, I, O, U, .

            if (ble1.equals(BLE.SEMCIRC_VERT_LEFT_CCW)) {
                return "C";
            }
            if(ble1.equals(BLE.HOOK_BOTTOM_LEFT_DOWN)){
                return "J";
            }
            if (isDownward(ble1)) {
                return "I";
            }
            if (ble1.equals(BLE.CIRC_CCW_TR)) {
                return "O";
            }
            if (ble1.equals(BLE.DOT)) {
                return ".";
            }
            if (ble1.equals(BLE.SEMCIRC_HORIZ_DOWN_CCW)) {
                return "U";
            }

        } else if (size == 2) { // D, J2, L, P, S, T, Q, V, X, Y1, G1

            if (isDownward(ble1) && isCW(ble2) || isUpward(ble1) && isCW(ble2)) {
                return "D/P";
            }
            if (isRightward(ble1) && isCW(ble2)) {
                return "J";
            }
            if (isDownward(ble1) && isRightward(ble2)) {
                return "L/T";
            }
            if (ble1.equals(BLE.SEMCIRC_VERT_LEFT_CCW) && isCW(ble2) || ble1.equals(BLE.HORIZ_FRTL) && isCW(ble2)) {
                return "S";
            }
            // if (isRightward() && ble2.equals(BLE.HORIZ_FLTR)) {
            // return "T";
            // }
            if (ble1.equals(BLE.CIRC_CCW_TR) && isRightward(ble2) || ble1.equals(BLE.CIRC_CCW_TR) && isDownward(ble2) ||
                    ble1.equals(BLE.SEMCIRC_HORIZ_DOWN_CCW) && isRightward(ble2) || ble1.equals(BLE.SEMCIRC_HORIZ_DOWN_CCW) && isDownward(ble2)) {
                return "Q";
            }
            if (isDownward(ble1) && isUpward(ble2)) {
                return "V";
            }
            if (isDownward(ble1) && isDownward(ble2)) {
                return "X/Y";
            }
            if (ble1.equals(BLE.SEMCIRC_VERT_LEFT_CCW)) {// && isRightward(ble2)) {// && isDownward(ble3)) {
                return "G";
            }

        } else if (size == 3) { // A, B, F, G2, H, K, N, R, Y2, Z1

            if (isUpward(ble1) && isDownward(ble2) && isRightward(ble3)
                    || isUpward(ble1) && isDownward(ble2) && ble3.equals(BLE.DOT)) { // =F
                return "A";
            }
            if (isDownward(ble1) && isCW(ble2) && isCW(ble3) || isUpward(ble1) && isCW(ble2) && isCW(ble3)) {
                return "B";
            }
            if (isDownward(ble1) && isRightward(ble2) && isRightward(ble3)
                    || isUpward(ble1) && isRightward(ble2) && isRightward(ble3)) {
                return "F";
            }
            if (ble1.equals(BLE.SEMCIRC_VERT_LEFT_CCW)) {// && isRightward(ble2)) {// && isDownward(ble3)) {
                return "G";
            }
            if (isDownward(ble1) && isDownward(ble2) && isRightward(ble3)) {
                return "H";
            }
            if (isDownward(ble1) && isLeftward(ble2) && isRightward(ble3)
                    || isDownward(ble1) && isDownward(ble2) && isDownward(ble3)
                    || isDownward(ble1) && isLeftward(ble2) && isDownward(ble3)) {
                return "K";
            }
            if (isUpward(ble1) && isDownward(ble2) && isUpward(ble3) || isUpward(ble1) && isDownward(ble2) && isUpward(ble3) || isUpward(ble1) && isRightward(ble2) && isUpward(ble3)) {
                return "N";
            }
            if (isDownward(ble1) && isCW(ble2) && isDownward(ble3)
                    || isDownward(ble1) && isCW(ble2) && isRightward(ble3)) {
                return "R";
            }
//            if (isDownward(ble1) && isUpward(ble2) && isDownward(ble3)) { // TODO element für DIAG: vert, diag, horiz
//                // einführen?
//                return "Y";
//            }
            if (isRightward(ble1) && isDownward(ble2) && isRightward(ble3)
                    || isRightward(ble1) && isLeftward(ble2) && isRightward(ble3)) {
                return "Z";
            }

        } else if (size == 4) { // E, M, W, Z2

            if (isDownward(ble1) && isRightward(ble2) && isRightward(ble3) && isRightward(ble4)
                    || isUpward(ble1) && isRightward(ble2) && isRightward(ble3) && isRightward(ble4)) {
                return "E";
            }
            if (isUpward(ble1) && isDownward(ble2) && isUpward(ble3) && isDownward(ble4)
                    || isUpward(ble1) && isRightward(ble2) && isRightward(ble3) && isDownward(ble4)
                    || isUpward(ble1) && isDownward(ble2) && isRightward(ble3) && isDownward(ble4)
                    || isUpward(ble1) && isRightward(ble2) && isUpward(ble3) && isDownward(ble4)
                    ||
                    isDownward(ble1) && isDownward(ble2) && isUpward(ble3) && isDownward(ble4)
                    || isDownward(ble1) && isRightward(ble2) && isRightward(ble3) && isDownward(ble4)
                    || isDownward(ble1) && isDownward(ble2) && isRightward(ble3) && isDownward(ble4)
                    || isDownward(ble1) && isRightward(ble2) && isUpward(ble3) && isDownward(ble4)
                    ) {

                return "M";
            }
            if (isDownward(ble1) && isUpward(ble2) && isDownward(ble3) && isUpward(ble4)
                    || isDownward(ble1) && isRightward(ble2) && isRightward(ble3) && isUpward(ble4)
                    || isDownward(ble1) && isRightward(ble2) && isDownward(ble3) && isUpward(ble4)
                    || isDownward(ble1) && isUpward(ble2) && isRightward(ble3) && isUpward(ble4)) {
                return "W";
            }
            if (isRightward(ble1) && isDownward(ble2) && isRightward(ble3) && isRightward(ble4)) {
                return "Z";
            }

        }
        return "?";


//        if (size == 1) { // C, I, O, U, .
//
//
//
//            if (ble1.equals(BLE.SEMCIRC_VERT_LEFT_CCW)) {
//                return "C";
//            }
//            if (ble1.equals(BLE.VERT_UP) || ble1.equals(BLE.VERT_DOWN)) {
//                return "I";
//            }
//            if (ble1.equals(BLE.CIRC_CCW_TR)) {
//                return "O";
//            }
//            if (ble1.equals(BLE.DOT)) {
//                return ".";
//            }
//
//
//
//
//        } else if (size == 2) { // D, J, L, P, S, T, Q, V, X, Y1
//
//
//            if (ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.SEMCIRC_VERT_RIGHT_CW)
//                    || ble1.equals(BLE.VERT_UP) && ble2.equals(BLE.SEMCIRC_VERT_RIGHT_CW)) {
//                return "D/P";
//            }
//            if (ble1.equals(BLE.HORIZ_FLTR) && ble2.equals(BLE.HOOK_BOTTOM_LEFT_DOWN)
//                    || ble1.equals(BLE.SEMCIRC_HORIZ_DOWN_CCW) && ble2.equals(BLE.HOOK_BOTTOM_LEFT_DOWN)) {
//                return "J";
//            }
//            if (ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.HORIZ_FLTR)
//                    || ble1.equals(BLE.DIAG_DOWN_FRTL) && ble2.equals(BLE.DIAG_DOWN_FLTR)) {
//                return "L";
//            }
//            if (ble1.equals(BLE.SEMCIRC_VERT_LEFT_CCW) && ble2.equals(BLE.SEMCIRC_VERT_RIGHT_CW)
//                    || ble1.equals(BLE.SEMCIRC_VERT_LEFT_CCW) && ble2.equals(BLE.HOOK_BOTTOM_LEFT_DOWN)
//                    || ble1.equals(BLE.HORIZ_FRTL) && ble2.equals(BLE.HOOK_BOTTOM_LEFT_DOWN)) {
//                return "S";
//            }
//            if (ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.HORIZ_FLTR)) {
//                return "T";
//            }
//            if (ble1.equals(BLE.CIRC_CCW_TR) && ble2.equals(BLE.DIAG_DOWN_FLTR)) {
//                return "Q";
//            }
//            if (ble1.equals(BLE.DIAG_DOWN_FLTR) && ble2.equals(BLE.DIAG_UP_FLTR)) {
//                return "V";
//            }
//            if (ble1.equals(BLE.DIAG_DOWN_FLTR) && ble2.equals(BLE.DIAG_DOWN_FRTL)) {
//                return "X/Y";
//            }
//
//
//
//
//        } else if (size == 3) { //A, B, F, G2, H, K, N, R, Y2, Z1
//
//
//            if (ble1.equals(BLE.DIAG_UP_FLTR) && ble2.equals(BLE.DIAG_DOWN_FLTR) && ble3.equals(BLE.HORIZ_FLTR)
//                    ||
//                    ble1.equals(BLE.DIAG_UP_FLTR) && ble2.equals(BLE.VERT_DOWN) && ble3.equals(BLE.HORIZ_FLTR)) {
//                return "A";
//            }
//            if (ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.SEMCIRC_VERT_RIGHT_CW) && ble3.equals(BLE.SEMCIRC_VERT_RIGHT_CW)
//                    ||
//                    ble1.equals(BLE.VERT_UP) && ble2.equals(BLE.SEMCIRC_VERT_RIGHT_CW) && ble3.equals(BLE.SEMCIRC_VERT_RIGHT_CW)
//                    ||
//                    ble1.equals(BLE.VERT_UP) && ble2.equals(BLE.SEMCIRC_VERT_RIGHT_CW) && ble3.equals(BLE.HOOK_BOTTOM_LEFT_DOWN)) {
//                return "B";
//            }
//            if (ble1.equals(BLE.VERT_UP) && ble2.equals(BLE.HORIZ_FLTR) && ble3.equals(BLE.HORIZ_FLTR)
//                    ||
//                    ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.HORIZ_FLTR) && ble3.equals(BLE.HORIZ_FLTR)) {
//                return "F";
//            }
//            if (ble1.equals(BLE.SEMCIRC_VERT_LEFT_CCW) && ble2.equals(BLE.HORIZ_FLTR) && ble3.equals(BLE.VERT_DOWN)) {
//                return "G";
//            }
//            if (ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.VERT_DOWN) && ble3.equals(BLE.HORIZ_FLTR)) {
//                return "H";
//            }
//            if (ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.DIAG_DOWN_FRTL) && ble3.equals(BLE.DIAG_DOWN_FLTR)
//                    ||
//                    ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.HORIZ_FRTL) && ble3.equals(BLE.DIAG_DOWN_FLTR)) {
//                return "K";
//            }
//            if (ble1.equals(BLE.VERT_UP) && ble2.equals(BLE.DIAG_DOWN_FLTR) && ble3.equals(BLE.VERT_UP)) {
//                return "N";
//            }
//            if (ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.SEMCIRC_VERT_RIGHT_CW) && ble3.equals(BLE.DIAG_DOWN_FLTR)
//                    ||
//                    ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.SEMCIRC_VERT_RIGHT_CW) && ble3.equals(BLE.VERT_DOWN )) {
//                return "R";
//            }
//            if (ble1.equals(BLE.DIAG_DOWN_FLTR) && ble2.equals(BLE.DIAG_UP_FLTR) && ble3.equals(BLE.DIAG_DOWN_FRTL)) {
//                return "Y";
//            }
//            if (ble1.equals(BLE.HORIZ_FLTR) && ble2.equals(BLE.DIAG_DOWN_FRTL) && ble3.equals(BLE.HORIZ_FLTR)) {
//                return "Z";
//            }
//
//
//
//        } else if (size == 4) { // E, M, W, Z2
//
//
//            if (ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.HORIZ_FLTR) && ble3.equals(BLE.HORIZ_FLTR) && ble4.equals(BLE.HORIZ_FLTR)
//                    ||
//                    ble1.equals(BLE.VERT_UP) && ble2.equals(BLE.HORIZ_FLTR) && ble3.equals(BLE.HORIZ_FLTR) && ble4.equals(BLE.HORIZ_FLTR)) {
//                return "E";
//            }
//            if (ble1.equals(BLE.VERT_UP) && ble2.equals(BLE.DIAG_DOWN_FLTR) && ble3.equals(BLE.DIAG_UP_FLTR) && ble4.equals(BLE.VERT_DOWN)) {
//                return "M";
//            }
//            if (ble1.equals(BLE.DIAG_DOWN_FLTR) && ble2.equals(BLE.DIAG_UP_FLTR) && ble3.equals(BLE.DIAG_DOWN_FLTR) && ble4.equals(BLE.DIAG_UP_FLTR)
//                    ||
//                    ble1.equals(BLE.VERT_DOWN) && ble2.equals(BLE.DIAG_UP_FLTR) && ble3.equals(BLE.DIAG_DOWN_FLTR) && ble4.equals(BLE.VERT_UP)
//                    ) {
//                return "W";
//            }
//            if (ble1.equals(BLE.DIAG_UP_FLTR) && ble2.equals(BLE.DIAG_DOWN_FLTR) && ble3.equals(BLE.DIAG_UP_FLTR) && ble4.equals(BLE.DIAG_DOWN_FLTR)
//                    ||
//                    ble1.equals(BLE.VERT_UP) && ble2.equals(BLE.DIAG_DOWN_FLTR) && ble3.equals(BLE.DIAG_UP_FLTR) && ble4.equals(BLE.VERT_DOWN)
//                    ) {
//                return "M";
//            }
//
//
//
//        }
//        return "?";


    }


    private static BLE getBLEName(int index) {
        List<String> labelNames = new ArrayList<String>(13);
        labelNames.add("CIRC_CCW_TR");
        labelNames.add("DIAG_DOWN_FLTR");
        labelNames.add("DIAG_DOWN_FRTL");
        labelNames.add("DIAG_UP_FLTR");
        labelNames.add("DOT");
        labelNames.add("HOOK_BOTTOM_LEFT_DOWN");
        labelNames.add("HORIZ_FLTR");
        labelNames.add("HORIZ_FRTL");
        labelNames.add("SEMCIRC_HORIZ_DOWN_CCW");
        labelNames.add("SEMCIRC_VERT_LEFT_CCW");
        labelNames.add("SEMCIRC_VERT_RIGHT_CW");
        labelNames.add("VERT_DOWN");
        labelNames.add("VERT_UP");

        return BLE.valueOf(labelNames.get(index));
    }


}
