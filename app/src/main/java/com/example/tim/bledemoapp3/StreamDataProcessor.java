package com.example.tim.bledemoapp3;

import android.app.Activity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tim.bledemoapp3.thiemo.SessionData;
import com.stabilo.digipenkit.StreamData;

//import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
//import org.deeplearning4j.util.ModelSerializer;
//import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
//import org.nd4j.linalg.dataset.api.preprocessor.serializer.NormalizerSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Provides means to process the incoming stream data from the pen
 */
public class StreamDataProcessor {

    private static int strokeCount = 0;
    private SessionData sessionData;
    private String filename = "";

    private TrainedNet trainedNet;

    private final static String STREAM_DATA_FILE = "/streamData";

    public static int label1 = -1;
    public static int label2 = -1;
    public static int label3 = -1;
    public static int label4 = -1;

    /**
     * Constructor
     *
     * @param activity    the activity to show the results in
     * @param sessionData the session data
     */
    public StreamDataProcessor(Activity activity, SessionData sessionData) {
        this.sessionData = sessionData;
        trainedNet = new TrainedNet(sessionData.folder.toString(), activity);
    }

    /**
     * Processes the given stream data
     *
     * @param streamData the incoming stream data
     * @param activity   the activity to show the results in
     */
    void setStreamData(StreamData streamData, Activity activity) {

        double pres = streamData.getPres().getValue();

        if (pres > 0.1) {//pen touching the paper
            filename = STREAM_DATA_FILE + strokeCount + ".csv";
            writeStreamDataFile(sessionData.folder, filename, streamData);
            LettersDemoActivity.lastEvent = new Date();
        } else {//pen lifted off the paper

            File file = new File(sessionData.folder + filename);

            if (file.exists() && !file.isDirectory()) {

//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
                LettersDemoActivity.lastEvent = new Date();
                if (activity.getClass().getSimpleName().equals("LettersDemoActivity")) {
                    List<TrainedNet.BleWithProb> results = null;
                    try {
                        results = trainedNet.useTrainedNet(file, 0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    final double highestIndexFinal = results.get(0).getIndex();
                    final double largestProbFinal = results.get(0).getProbability();


                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // if (activity.getClass().getSimpleName().equals("LettersDemoActivity")) {
                            ImageView ble1 = activity.findViewById(R.id.bleImage1);
                            ImageView ble2 = activity.findViewById(R.id.bleImage2);
                            ImageView ble3 = activity.findViewById(R.id.bleImage3);
                            ImageView ble4 = activity.findViewById(R.id.bleImage4);

                            TextView cert1 = activity.findViewById(R.id.certainty1);
                            TextView cert2 = activity.findViewById(R.id.certainty2);
                            TextView cert3 = activity.findViewById(R.id.certainty3);
                            TextView cert4 = activity.findViewById(R.id.certainty4);

                            TextView reconstructedLetter = activity.findViewById(R.id.reconstructedLetter);

                            String imageName = "ble" + (int) highestIndexFinal;
                            int drawableId = activity.getResources().getIdentifier(imageName, "mipmap", activity.getPackageName());

                            if (label1 == -1) {
                                ble1.setImageResource(drawableId);
                                label1 = (int) highestIndexFinal;
                                cert1.setText((int) (largestProbFinal * 100) + "%");
                            } else if (label2 == -1) {
                                ble2.setImageResource(drawableId);
                                label2 = (int) highestIndexFinal;
                                cert2.setText((int) (largestProbFinal * 100) + "%");
                            } else if (label3 == -1) {
                                ble3.setImageResource(drawableId);
                                label3 = (int) highestIndexFinal;
                                cert3.setText((int) (largestProbFinal * 100) + "%");
                            } else if (label4 == -1) {
                                ble4.setImageResource(drawableId);
                                label4 = (int) highestIndexFinal;
                                cert4.setText((int) (largestProbFinal * 100) + "%");
                            } else {

                                //button flash
                                Button nextLetterButton = activity.findViewById(R.id.nextLetterButton);
                                Animation mAnimation = new AlphaAnimation(1, 0);
                                mAnimation.setDuration(100);
                                mAnimation.setInterpolator(new LinearInterpolator());
                                mAnimation.setRepeatCount(5);
                                mAnimation.setRepeatMode(Animation.REVERSE);
                                nextLetterButton.startAnimation(mAnimation);
                            }

                            String wholeLetter = LetterReconstruction.reconstruct(label1, label2, label3, label4);
                            reconstructedLetter.setText(wholeLetter);

                        }
                    });

                    file.delete();
                    //}

                } else { //BLE SPLITTER Activity

                    List<TrainedNet.BleWithProb> results = null;
                    try {
                        results = trainedNet.useTrainedNet(file, BleSplitterDemoActivity.separations);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    final List<TrainedNet.BleWithProb> finalResults = results;

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (BleSplitterDemoActivity.separations == -1) {
                                showRightImages(finalResults.size());
                            }


                            ImageView ble1 = activity.findViewById(R.id.bleImage1);
                            TextView certainty1 = activity.findViewById(R.id.certainty1);
                            certainty1.setText((int) (finalResults.get(0).getProbability() * 100) + "%");
                            String imageName1 = "ble" + finalResults.get(0).getIndex();
                            int drawableId1 = activity.getResources().getIdentifier(imageName1, "mipmap", activity.getPackageName());
                            ble1.setImageResource(drawableId1);


                            if (finalResults.size() > 1) {
                                ImageView ble2 = activity.findViewById(R.id.bleImage2);
                                TextView certainty2 = activity.findViewById(R.id.certainty2);
                                certainty2.setText((int) (finalResults.get(1).getProbability() * 100) + "%");
                                String imageName2 = "ble" + finalResults.get(1).getIndex();
                                int drawableId2 = activity.getResources().getIdentifier(imageName2, "mipmap", activity.getPackageName());
                                ble2.setImageResource(drawableId2);

                            }

                            if (finalResults.size() > 2) {
                                ImageView ble3 = activity.findViewById(R.id.bleImage3);
                                TextView certainty3 = activity.findViewById(R.id.certainty3);
                                certainty3.setText((int) (finalResults.get(2).getProbability() * 100) + "%");
                                String imageName3 = "ble" + (int) finalResults.get(2).getIndex();
                                int drawableId3 = activity.getResources().getIdentifier(imageName3, "mipmap", activity.getPackageName());
                                ble3.setImageResource(drawableId3);
                            }

                            if (finalResults.size() > 3) {
                                ImageView ble4 = activity.findViewById(R.id.bleImage4);
                                TextView certainty4 = activity.findViewById(R.id.certainty4);
                                certainty4.setText((int) (finalResults.get(3).getProbability() * 100) + "%");
                                String imageName4 = "ble" + (int) finalResults.get(3).getIndex();
                                int drawableId4 = activity.getResources().getIdentifier(imageName4, "mipmap", activity.getPackageName());
                                ble4.setImageResource(drawableId4);
                            }
                        }

                        private void showRightImages(int size) {
                            ImageView ble2 = activity.findViewById(R.id.bleImage2);
                            TextView certainty2 = activity.findViewById(R.id.certainty2);
                            ImageView ble3 = activity.findViewById(R.id.bleImage3);
                            TextView certainty3 = activity.findViewById(R.id.certainty3);
                            ImageView ble4 = activity.findViewById(R.id.bleImage4);
                            TextView certainty4 = activity.findViewById(R.id.certainty4);

                            if (size == 1) {

                                ble2.setVisibility(View.INVISIBLE);
                                certainty2.setVisibility(View.INVISIBLE);
                                ble3.setVisibility(View.INVISIBLE);
                                certainty3.setVisibility(View.INVISIBLE);
                                ble4.setVisibility(View.INVISIBLE);
                                certainty4.setVisibility(View.INVISIBLE);

                            } else if (size == 2) {

                                ble2.setVisibility(View.VISIBLE);
                                certainty2.setVisibility(View.VISIBLE);
                                ble3.setVisibility(View.INVISIBLE);
                                certainty3.setVisibility(View.INVISIBLE);
                                ble4.setVisibility(View.INVISIBLE);
                                certainty4.setVisibility(View.INVISIBLE);
                            } else if (size == 3) {

                                ble2.setVisibility(View.VISIBLE);
                                certainty2.setVisibility(View.VISIBLE);
                                ble3.setVisibility(View.VISIBLE);
                                certainty3.setVisibility(View.VISIBLE);
                                ble4.setVisibility(View.INVISIBLE);
                                certainty4.setVisibility(View.INVISIBLE);
                            } else if (size == 4) {
                                ble2.setVisibility(View.VISIBLE);
                                certainty2.setVisibility(View.VISIBLE);
                                ble3.setVisibility(View.VISIBLE);
                                certainty3.setVisibility(View.VISIBLE);
                                ble4.setVisibility(View.VISIBLE);
                                certainty4.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    //  }
                    // }).start();

                    file.delete();
                }
                strokeCount++;
            }
        }
    }

    /**
     * Writes the given streamData to a .csv file
     *
     * @param folder     the folder to write to
     * @param filename   the file name to write to
     * @param streamData the streamdata to write
     */
    private void writeStreamDataFile(File folder, String filename, StreamData streamData) {
        try {
            File path = new File(folder, filename);
            System.out.println(path.getAbsolutePath());

            // Check if the File is there (for setting the headline)
            path.getParentFile().mkdirs();
            boolean isANewFile = path.createNewFile();

            FileOutputStream stream = new FileOutputStream(path, true);

            if (path.exists() && !path.isDirectory()) {

                if (isANewFile) {
                    stream.write(getHeadline().getBytes());
                }


                stream.write((streamData.getQ0().getValue() + ";").getBytes());
                stream.write((streamData.getQ1().getValue() + ";").getBytes());
                stream.write((streamData.getQ2().getValue() + ";").getBytes());
                stream.write((streamData.getQ3().getValue() + ";").getBytes());

                stream.write((streamData.getPres().getValue() + ";").getBytes());

                stream.write((streamData.getTx().getValue() + ";").getBytes());
                stream.write((streamData.getTy().getValue() + ";").getBytes());
                stream.write((streamData.getTz().getValue() + ";").getBytes());

                stream.write((streamData.getZ0().getValue() + ";").getBytes()); //acc
                stream.write((streamData.getZ1().getValue() + ";").getBytes());
                stream.write((streamData.getZ2().getValue() + ";").getBytes());
                stream.write((streamData.getZ3().getValue() + ";").getBytes()); //gyro
                stream.write((streamData.getZ4().getValue() + ";").getBytes());
                stream.write((streamData.getZ5().getValue() + ";").getBytes());
//                stream.write((streamData.getZ6().getValue() + ";").getBytes()); //magnetometer
//                stream.write((streamData.getZ7().getValue() + ";").getBytes());
//                stream.write((streamData.getZ8().getValue() + "").getBytes());
                stream.write((streamData.getWz().getValue() + ";").getBytes()); //angle


                stream.write("\n".getBytes());

                stream.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getHeadline() {
        return "q0;q1;q2;q3;pres;tx;ty;tz;z0;z1;z2;z3;z4;z5;wz\n";
    }
}
