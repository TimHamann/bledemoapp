package com.example.tim.bledemoapp3;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.tim.bledemoapp3.thiemo.BLEConnector;
import com.example.tim.bledemoapp3.thiemo.ButtonState;
import com.example.tim.bledemoapp3.thiemo.ConnectionHolder;
import com.example.tim.bledemoapp3.thiemo.DigipenCalibrator;
import com.example.tim.bledemoapp3.thiemo.DigipenFinder;
import com.example.tim.bledemoapp3.thiemo.SessionData;
import com.example.tim.bledemoapp3.thiemo.Tools;
import com.stabilo.digipenkit.DigipenCalibration;
import com.stabilo.digipenkit.StreamData;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * The app's entry point
 */
public class WelcomeScreenActivity extends AppCompatActivity implements View.OnClickListener, ConnectionHolder.DigipenConnect{
    ConnectionHolder connectionHolder;
    SessionData sessionData;

    final int REQUEST_DIGIPENFINDER = 0;
    final int REQUEST_DIGIPENCALIBRATOR = 1;
    final int REQUEST_FILE_WRITE_PERMISSION = 2;

    ProgressBar pb_battery;
    Button connectButton;
    Button calibrateButton;
    //Button bleDemoButton;
    Button lettersDemoButton;
    Button splitterButton;

    TextView penName;
    TextView serialNumber;
    TextView calibrationState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);


        // init progressBar
        pb_battery = findViewById(R.id.pb_battery);

        connectButton = findViewById(R.id.connectButton);
        connectButton.setOnClickListener(this);
        calibrateButton = findViewById(R.id.calibrateButton);
        calibrateButton.setOnClickListener(this);
        calibrateButton.setEnabled(false);
        //bleDemoButton = findViewById(R.id.bleDemoButton);
        //bleDemoButton.setOnClickListener(this);
        //bleDemoButton.setEnabled(false);
        lettersDemoButton = findViewById(R.id.lettersDemoButton);
        lettersDemoButton.setOnClickListener(this);
        lettersDemoButton.setEnabled(false);
        splitterButton = findViewById(R.id.splitterButton);
        splitterButton.setOnClickListener(this);
        splitterButton.setEnabled(false);

        penName = findViewById(R.id.penName);
        serialNumber = findViewById(R.id.serialNumber);
        calibrationState = findViewById(R.id.calibrationState);


        connectionHolder = ((ConnectionHolder) this.getApplication());

        File folder = new File(Environment.getExternalStorageDirectory() + "/BLEDemoApp/");
        boolean madeDirs = folder.mkdirs();


        File streamData = new File(folder.getAbsolutePath() + "/streamData.csv");
        boolean deleted = streamData.delete();
        System.out.println("DELETED STREAMDATA FILE: " + deleted + " " + streamData.getAbsolutePath());
        //TODO calibration file?

    }

    @Override
    protected void onResume() {
        super.onResume();

        //if (checkRequirement()) {

        sessionData = connectionHolder.sessionData;

        // Fill the app version in the sessionData object
        sessionData.appVersion = BuildConfig.VERSION_NAME;

        Calendar calendar = Calendar.getInstance();

        Date date;

        if (sessionData.date != null) {
            date = sessionData.date;
        } else {
            date = calendar.getTime();
            sessionData.date = date;
        }
        sessionData.folder = new File(Environment.getExternalStorageDirectory() + "/BLEDemoApp/");

        // Set interface executive
        connectionHolder.setInterface(this);


        on_digipenIsConnected(connectionHolder.PEN_IS_CONNECTED);

        connectionHolder.getDigipenInformation();

        //connectionHolder.autoConnectDigipen();
        //}

        //delete remaining files
        File folder = new File(Environment.getExternalStorageDirectory() + "/BLEDemoApp/");
        File[] files = folder.listFiles();
        if(files != null){
            for (File entry: files
                    ) {
                if(entry.getName().contains(".csv") && !entry.getName().contains("labelDummy")){
                    entry.delete();
                }
            }
        }

    }

    @Override
    public void onClick(View view) {
        System.out.println("switch statement...");
        switch (view.getId()) {
            case R.id.connectButton:
                System.out.println("connecting");

                if (connectionHolder.PEN_IS_CONNECTED) {
                    connectionHolder.disconnectDigipen();
                    Tools.setText(connectionHolder.PEN_IS_CONNECTED, "CONNECT", connectButton, this);

                } else {

                    Intent digipenFinder = new Intent(WelcomeScreenActivity.this, DigipenFinder.class);
                    digipenFinder.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivityForResult(digipenFinder, REQUEST_DIGIPENFINDER);
                    Tools.setText(connectionHolder.PEN_IS_CONNECTED, "working...", connectButton, this);
                }

                break;
            case R.id.calibrateButton:
                System.out.println("calibrating");
                if (connectionHolder.PEN_IS_CONNECTED) {

                    Intent digipenFinder = new Intent(WelcomeScreenActivity.this, DigipenCalibrator.class);
                    digipenFinder.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivityForResult(digipenFinder, REQUEST_DIGIPENCALIBRATOR);
                }
                break;
            //case R.id.bleDemoButton:
             //   System.out.println("bleDemoButton");
            //    Intent myIntent = new Intent(WelcomeScreenActivity.this, BLEDemoActivity.class);
            //    startActivity(myIntent);
            //    break;
            case R.id.lettersDemoButton:
                System.out.println("lettersDemoButton");
                Intent lettersIntent = new Intent(WelcomeScreenActivity.this, LettersDemoActivity.class);
                startActivity(lettersIntent);
                break;
            case R.id.splitterButton:
                System.out.println("SplitterDemoButton");
                Intent doubleBleIntent = new Intent(WelcomeScreenActivity.this, BleSplitterDemoActivity.class);
                startActivity(doubleBleIntent);
                break;
            default:
                System.out.println("button not found");
                break;
        }
    }

//    /**
//     * wird aufgerufen, wenn geräte gefunden wurden. gibt liste aller geräte zurück
//     * @param bleDevices
//     */
//    @Override
//    public void onBLEScanResult(HashSet<BLEConnector.BLEDevice> bleDevices) {
//        //... BLEDevice.Address gibt BluetoothDevice zurück
//    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_DIGIPENFINDER) {

            if (resultCode == RESULT_OK) {

                BLEConnector.BLEDevice bleDevice = connectionHolder.getBleDevice();
                connectionHolder.connectDigipen(bleDevice);
            }
        }
    }
    @Override
    public void on_digipenIsConnected(boolean connected) {
        if (connected) {
            System.out.println("Pen is connected");
            Tools.setText(connectionHolder.PEN_IS_CONNECTED, "DISCONNECT", connectButton, this);

            Tools.setButtonState(calibrateButton, ButtonState.enabled, this);
            //Tools.setButtonState(bleDemoButton, ButtonState.enabled, this);
            Tools.setButtonState(lettersDemoButton, ButtonState.enabled, this);
            Tools.setButtonState(splitterButton, ButtonState.enabled, this);


//            bleDemoButton.setEnabled(true);
//            lettersDemoButton.setEnabled(true);
//            calibrateButton.setEnabled(true);
//
//            bleDemoButton.invalidate();
//            lettersDemoButton.invalidate();
//            calibrateButton.invalidate();

        } else {
            System.out.println("Pen is disconnected");
            //Tools.setVisibility(View.INVISIBLE, pb_spinner, this);

            Tools.setButtonState(calibrateButton, ButtonState.disabled, this);
            //Tools.setButtonState(bleDemoButton, ButtonState.disabled, this);
            Tools.setButtonState(lettersDemoButton, ButtonState.disabled, this);
            Tools.setButtonState(splitterButton, ButtonState.disabled, this);

            resetData();
        }
    }

    @Override
    public void on_digipenBatteryHasChanged(int battery) {
        System.out.println("battery");
        Tools.setProgressBarValue(connectionHolder.PEN_IS_CONNECTED, battery, pb_battery, this);
        sessionData.digipenBattery = battery;
    }

    @Override
    public void on_digipenNameHasChanged(String name) {
        Tools.setText(connectionHolder.PEN_IS_CONNECTED, name, penName, this);
        sessionData.digipenName = name;
    }

    @Override
    public void on_digipenFirmwareVersionHasChanged(String firmware) {
        sessionData.digipenFirmwareVersion = firmware;
    }

    @Override
    public void on_digipenSerialHasChanged(String serial) {
        Tools.setText(connectionHolder.PEN_IS_CONNECTED, serial, serialNumber, this);
        sessionData.digipenSerial = serial;
    }

    @Override
    public void on_digipenLastCalibrationDateChanged(Date date) {
        Date today = new Date();
        String string;

        long daysSinceCalibration = Tools.getDateDifference(date, today, TimeUnit.DAYS);

        Tools.setText(connectionHolder.PEN_IS_CONNECTED, daysSinceCalibration + " days since the last calibration.", calibrationState, this);

        sessionData.calibrationDate = date;
    }

    @Override
    public void on_digipenCalibration(DigipenCalibration.CalibrationState calibrationState, Double stepProgress, DigipenCalibration.CalibrationAction calibrationAction) {

    }

    @Override
    public void on_digipenDataStream(StreamData streamData) {

    }

    @Override
    public void on_digipenOffPaper() {

    }

    @Override
    public void on_digipenIsNotStreaming() {

    }

    private void resetData() {
        on_digipenBatteryHasChanged(0);
        on_digipenLastCalibrationDateChanged(new Date());
        on_digipenNameHasChanged("");
        on_digipenSerialHasChanged("");
    }
}
