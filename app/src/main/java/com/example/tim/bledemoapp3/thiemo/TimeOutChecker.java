package com.example.tim.bledemoapp3.thiemo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by thiemodaubner on 14.03.18.
 * Cool
 */

class TimeOutChecker {

    private TimeOut timeOut;

    interface TimeOut {
        void on_timeOut();
    }

    TimeOutChecker(TimeOut timeOut) {
        this.timeOut = timeOut;
    }

    private HashMap<String, Boolean> checkMap = new HashMap<>();
    private ArrayList<Thread> threads = new ArrayList<>();

    boolean isChecking = false;

    void checkForStream(final String inputName, final int timeInMilliseconds) {

        if (isChecking) {

            checkMap.put(inputName, false);

            Thread thread = new Thread() {
                public void run() {

                    try {

                        // Sleep for given digipenStartTime
                        Thread.sleep(timeInMilliseconds);

                        // Take the value of the checker
                        boolean itIsDone = checkMap.remove(inputName);

                        // Check the value
                        if (itIsDone) {

                            // When the job is done, destroy the thread
                            this.interrupt();

                        } else {

                            // When the job is not done, call the callback
                            timeOut.on_timeOut();
                            this.interrupt();

                        }

                    } catch (Exception ignore) {
                    }
                }
            };

            threads.add(thread);

            thread.start();
        }
    }

    void itIsDone(String name) {
        checkMap.put(name, true);
    }

    void clear() {

        isChecking = false;

        checkMap.clear();
        try {
            for (Thread thread : threads) {
                thread.interrupt();
            }
        } catch (Exception ignore) {
        }
    }
}
