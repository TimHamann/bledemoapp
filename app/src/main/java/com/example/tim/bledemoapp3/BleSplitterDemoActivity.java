package com.example.tim.bledemoapp3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.tim.bledemoapp3.thiemo.ConnectionHolder;
import com.example.tim.bledemoapp3.thiemo.SessionData;
import com.example.tim.bledemoapp3.thiemo.Tools;
import com.stabilo.digipenkit.DigipenCalibration;
import com.stabilo.digipenkit.StreamData;

import java.util.Date;

/**
 * Performs the BLE Splitter Demonstration Activity
 */
public class BleSplitterDemoActivity extends AppCompatActivity implements ConnectionHolder.DigipenConnect{
    ConnectionHolder connectionHolder;
    SessionData sessionData;
    StreamDataProcessor streamDataProcessor;

    ProgressBar bleDemoBattery;
    Spinner spinner;
    public static int separations;

    ImageView ble2;
    TextView cert2;
    ImageView ble3;
    TextView cert3;
    ImageView ble4;
    TextView cert4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ble_splitter_demo);

        bleDemoBattery = findViewById(R.id.bleDemoBattery);
        connectionHolder = ((ConnectionHolder) this.getApplication());

        spinner = findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new MyItemSelectedListener());


        ble2 = findViewById(R.id.bleImage2);
        cert2 = findViewById(R.id.certainty2);

        if(separations == 1){
            ble2.setVisibility(View.INVISIBLE);
            cert2.setVisibility(View.INVISIBLE);
        }

        ble3 = findViewById(R.id.bleImage3);
        cert3 = findViewById(R.id.certainty3);

        if(separations == 2){
            ble3.setVisibility(View.INVISIBLE);
            cert3.setVisibility(View.INVISIBLE);
        }

        ble4 = findViewById(R.id.bleImage4);
        cert4 = findViewById(R.id.certainty4);

        if(separations == 3){
            ble4.setVisibility(View.INVISIBLE);
            cert4.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        //if (checkRequirement()) {
        connectionHolder = ((ConnectionHolder) this.getApplication());
        sessionData = connectionHolder.sessionData;

        // Fill the app version in the sessionData object
        sessionData.appVersion = BuildConfig.VERSION_NAME;


        // Set interface executive
        connectionHolder.setInterface(this);
        on_digipenIsConnected(connectionHolder.PEN_IS_CONNECTED);

        connectionHolder.getDigipenInformation();

        //connectionHolder.autoConnectDigipen();
        //}


        streamDataProcessor = new  StreamDataProcessor(this, sessionData);

        connectionHolder.startDigipenStream();



    }

    @Override
    protected void onPause() {

        super.onPause();
        connectionHolder.stopDigipenStream();
    }

    @Override
    public void on_digipenIsConnected(boolean connected) {
        if (!connected) {
            Intent myIntent = new Intent(BleSplitterDemoActivity.this, WelcomeScreenActivity.class);
            myIntent.putExtra("mode", "BLEs"); //Optional parameters
            BleSplitterDemoActivity.this.startActivity(myIntent);
        }
    }

    @Override
    public void on_digipenBatteryHasChanged(int battery) {
        System.out.println("battery");
        Tools.setProgressBarValue(connectionHolder.PEN_IS_CONNECTED, battery, bleDemoBattery, this);
        sessionData.digipenBattery = battery;
    }

    @Override
    public void on_digipenNameHasChanged(String name) {
    }

    @Override
    public void on_digipenFirmwareVersionHasChanged(String firmware) {

    }

    @Override
    public void on_digipenSerialHasChanged(String serial) {

    }

    @Override
    public void on_digipenLastCalibrationDateChanged(Date date) {

    }

    @Override
    public void on_digipenCalibration(DigipenCalibration.CalibrationState calibrationState, Double stepProgress, DigipenCalibration.CalibrationAction calibrationAction) {
    }

    @Override
    public void on_digipenDataStream(StreamData streamData) {
        streamDataProcessor.setStreamData(streamData, this);
    }

    @Override
    public void on_digipenOffPaper() {

    }

    @Override
    public void on_digipenIsNotStreaming() {

    }

    private class MyItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            String selected = parent.getItemAtPosition(pos).toString();
            if(selected.equals("Single BLE")){
                separations = 0;
                ble2.setVisibility(View.INVISIBLE);
                cert2.setVisibility(View.INVISIBLE);
                ble3.setVisibility(View.INVISIBLE);
                cert3.setVisibility(View.INVISIBLE);
                ble4.setVisibility(View.INVISIBLE);
                cert4.setVisibility(View.INVISIBLE);
            }else if(selected.equals("Double BLE")){
                separations = 1;
                ble2.setVisibility(View.VISIBLE);
                cert2.setVisibility(View.VISIBLE);
                ble3.setVisibility(View.INVISIBLE);
                cert3.setVisibility(View.INVISIBLE);
                ble4.setVisibility(View.INVISIBLE);
                cert4.setVisibility(View.INVISIBLE);
            }else if (selected.equals("Triple BLE")){
                separations = 2;
                ble2.setVisibility(View.VISIBLE);
                cert2.setVisibility(View.VISIBLE);
                ble3.setVisibility(View.VISIBLE);
                cert3.setVisibility(View.VISIBLE);
                ble4.setVisibility(View.INVISIBLE);
                cert4.setVisibility(View.INVISIBLE);
            }else if (selected.equals("Quadruple BLE")){
                separations = 3;
                ble2.setVisibility(View.VISIBLE);
                cert2.setVisibility(View.VISIBLE);
                ble3.setVisibility(View.VISIBLE);
                cert3.setVisibility(View.VISIBLE);
                ble4.setVisibility(View.VISIBLE);
                cert4.setVisibility(View.VISIBLE);
            }else if (selected.equals("AUTO MODE")){
                separations = -1;
                ble2.setVisibility(View.VISIBLE);
                cert2.setVisibility(View.VISIBLE);
                ble3.setVisibility(View.VISIBLE);
                cert3.setVisibility(View.VISIBLE);
                ble4.setVisibility(View.VISIBLE);
                cert4.setVisibility(View.VISIBLE);
            }
        }

        public void onNothingSelected(AdapterView parent) {
            // Do nothing.
        }
    }
}
