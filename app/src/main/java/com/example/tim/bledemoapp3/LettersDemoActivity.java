package com.example.tim.bledemoapp3;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.tim.bledemoapp3.thiemo.ConnectionHolder;
import com.example.tim.bledemoapp3.thiemo.SessionData;
import com.example.tim.bledemoapp3.thiemo.Tools;
import com.stabilo.digipenkit.DigipenCalibration;
import com.stabilo.digipenkit.StreamData;

import java.sql.Timestamp;
import java.util.Date;
import java.util.stream.Stream;

/**
 * Does the letter reconstruction demonstration (with discontinued strokes)
 */
public class LettersDemoActivity extends AppCompatActivity implements ConnectionHolder.DigipenConnect {
    ConnectionHolder connectionHolder;
    SessionData sessionData;
    StreamDataProcessor streamDataProcessor;

    ProgressBar bleDemoBattery;
    ImageView ble1;
    ImageView ble2;
    ImageView ble3;
    ImageView ble4;
    Button nextLetterButton;

    TextView cert1;
    TextView cert2;
    TextView cert3;
    TextView cert4;

    public static Date lastEvent;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lettersdemo);

        bleDemoBattery = findViewById(R.id.bleDemoBattery);
        connectionHolder = ((ConnectionHolder) this.getApplication());
        TextView reconstructedLetter = findViewById(R.id.reconstructedLetter);

        ble1 = findViewById(R.id.bleImage1);
        ble2 = findViewById(R.id.bleImage2);
        ble3 = findViewById(R.id.bleImage3);
        ble4 = findViewById(R.id.bleImage4);

        cert1 = findViewById(R.id.certainty1);
        cert2 = findViewById(R.id.certainty2);
        cert3 = findViewById(R.id.certainty3);
        cert4 = findViewById(R.id.certainty4);


        nextLetterButton = findViewById(R.id.nextLetterButton);
        nextLetterButton.setOnClickListener(v -> {

            ble1.setImageResource(R.mipmap.none);
            ble2.setImageResource(R.mipmap.none);
            ble3.setImageResource(R.mipmap.none);
            ble4.setImageResource(R.mipmap.none);

            cert1.setText("%");
            cert2.setText("%");
            cert3.setText("%");
            cert4.setText("%");


            resetBLEs();


            reconstructedLetter.setText("?");

        });

//        Button reconstructButton = findViewById(R.id.reconstructLetterButton);
//        reconstructButton.setOnClickListener(v -> {
//            String result = LetterReconstruction.reconstruct(label1, label2, label3, label4);
//            reconstructedLetter.setText(result);
//            //nextLetterButton.callOnClick();
//        });

        lastEvent = new Date();

        //resets the fields, if no further input is given
        handler.postDelayed(runnable, 1000);
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
                Date current = new Date();
                long secondsDiff = (current.getTime() - lastEvent.getTime()) / 1000;
                //System.out.println("Difference in secs: " + secondsDiff);
                if (secondsDiff > 2) {
                    nextLetterButton.callOnClick();
                }

            handler.postDelayed(this, 1000);
        }
    };

    @Override
    protected void onResume() {
        handler.postDelayed(runnable, 1000);
        super.onResume();

        //if (checkRequirement()) {
        connectionHolder = ((ConnectionHolder) this.getApplication());
        sessionData = connectionHolder.sessionData;

        // Fill the app version in the sessionData object
        sessionData.appVersion = BuildConfig.VERSION_NAME;


        // Set interface executive
        connectionHolder.setInterface(this);
        on_digipenIsConnected(connectionHolder.PEN_IS_CONNECTED);

        connectionHolder.getDigipenInformation();

        //connectionHolder.autoConnectDigipen();
        //}


        streamDataProcessor = new StreamDataProcessor(this, sessionData);

        connectionHolder.startDigipenStream();

        resetBLEs();


    }

    private void resetBLEs() {

        streamDataProcessor.label1 = -1;
        streamDataProcessor.label2 = -1;
        streamDataProcessor.label3 = -1;
        streamDataProcessor.label4 = -1;

    }

    @Override
    protected void onPause() {

        super.onPause();
        connectionHolder.stopDigipenStream();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void on_digipenIsConnected(boolean connected) {
        if (!connected) {
            Intent myIntent = new Intent(LettersDemoActivity.this, WelcomeScreenActivity.class);
            myIntent.putExtra("mode", "BLEs"); //Optional parameters
            LettersDemoActivity.this.startActivity(myIntent);
        }
    }

    @Override
    public void on_digipenBatteryHasChanged(int battery) {
        System.out.println("battery");
        Tools.setProgressBarValue(connectionHolder.PEN_IS_CONNECTED, battery, bleDemoBattery, this);
        sessionData.digipenBattery = battery;
    }

    @Override
    public void on_digipenNameHasChanged(String name) {
    }

    @Override
    public void on_digipenFirmwareVersionHasChanged(String firmware) {

    }

    @Override
    public void on_digipenSerialHasChanged(String serial) {

    }

    @Override
    public void on_digipenLastCalibrationDateChanged(Date date) {

    }

    @Override
    public void on_digipenCalibration(DigipenCalibration.CalibrationState calibrationState, Double stepProgress, DigipenCalibration.CalibrationAction calibrationAction) {
    }

    @Override
    public void on_digipenDataStream(StreamData streamData) {
        streamDataProcessor.setStreamData(streamData, this);
    }

    @Override
    public void on_digipenOffPaper() {

    }

    @Override
    public void on_digipenIsNotStreaming() {

    }

}
