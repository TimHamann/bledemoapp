package com.example.tim.bledemoapp3.thiemo;

import java.io.File;

/**
 * Created by thiemodaubner on 01.03.18.
 * Cool
 */

class AppSettings {

    String leftHanded;
    String rightHanded;

    String genderMale;
    String genderFemale;

    //static LabelType labelType;

    int maxAgeInDays;

    int criticalBatteryPower;

    String voice_language;
    double voice_pitch;
    double voice_rate;

    int bearOut_minTimeInAir;
    int bearOut_timeInAir;
    int bearOut_maxTimeInAir;
    int bearOut_minimumForceThreshold;

    File representation_pictureFolder;

//    void setLabelType(boolean inPictures, String folder) {
//
//        if (inPictures) {
//
//            representation_pictureFolder = new File(Environment.getExternalStorageDirectory() + "/Pencorder/", folder);
//            representation_pictureFolder.mkdir();
//
//            labelType = LabelType.PICTURES;
//
//        } else {
//
//            labelType = LabelType.TEXT;
//        }
//    }
}
