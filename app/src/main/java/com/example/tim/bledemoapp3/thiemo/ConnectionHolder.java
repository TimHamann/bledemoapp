package com.example.tim.bledemoapp3.thiemo;

import android.annotation.SuppressLint;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import com.example.tim.bledemoapp3.R;
import com.stabilo.digipenkit.CalibrationData;
import com.stabilo.digipenkit.DeviceInformation;
import com.stabilo.digipenkit.DigipenCalibration;
import com.stabilo.digipenkit.DigipenKit;
import com.stabilo.digipenkit.Settings;
import com.stabilo.digipenkit.StreamData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

/**
 * Created by thiemodaubner on 22.02.18.
 * Cool
 */

public class ConnectionHolder extends Application implements DigipenKit.DigipenDataTransfer, PenBehaviorChecker.BehaviorChecker {

    public interface DigipenConnect {

        void on_digipenIsConnected(boolean connected);

        void on_digipenBatteryHasChanged(int battery);

        void on_digipenNameHasChanged(String name);

        void on_digipenFirmwareVersionHasChanged(String firmware);

        void on_digipenSerialHasChanged(String serial);

        void on_digipenLastCalibrationDateChanged(Date date);

        void on_digipenCalibration(DigipenCalibration.CalibrationState calibrationState, Double stepProgress, DigipenCalibration.CalibrationAction calibrationAction);

        void on_digipenDataStream(StreamData streamData);

        void on_digipenOffPaper();

        void on_digipenIsNotStreaming();
    }

    public interface PencoderApplication {

        void on_initError(String error);

        void on_foundPairedDevice(String device);
    }

    private final String DIGIPEN_MASK = "0000000000000000001111000010011100000111111100000000000000000000";

    DigipenConnect digipenConnect = null;
    PencoderApplication pencoderApplication = null;
    DigipenKit digipenKit = null;
    BLEConnector.BLEDevice bleDevice = null;
    AppSettings appSettings = null;
    public SessionData sessionData = new SessionData();
    //PenBehaviorChecker penBehaviorChecker;
    //LabelMachine labelMachine;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    DeviceInformation deviceInformation;

    public boolean PEN_IS_CONNECTED = false;
    public boolean PEN_IS_STREAMING = false;

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate() {

        super.onCreate();
        digipenKit = new DigipenKit(this, getApplicationContext(), DIGIPEN_MASK);
        DigipenKit.REFERENCE_DATA_VERSION = "0.3.0";

        sharedPref = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    public void setInterface(DigipenConnect digipenConnect) {
        this.digipenConnect = digipenConnect;
        //this.pencoderApplication = (PencoderApplication) digipenConnect;

        //this.getSettings();

        //penBehaviorChecker = new PenBehaviorChecker(this, appSettings.bearOut_minimumForceThreshold, appSettings.bearOut_timeInAir);
    }

    public BLEConnector.BLEDevice getBleDevice() {
        return bleDevice;
    }

    public void setBleDevice(BLEConnector.BLEDevice bleDevice) {

        this.bleDevice = bleDevice;

        if (bleDevice != null) {
            Tools.setPreference(getApplicationContext(), "bleDevice", bleDevice.address.toString());
        }
    }

    public void connectDigipen(BLEConnector.BLEDevice device) {
        digipenKit.close();
        digipenKit.connect(device.address);
    }

    public void autoConnectDigipen() {

        if (!PEN_IS_CONNECTED) {

            final String bleAddress = Tools.getPreference(getApplicationContext(), "bleDevice");

            Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
            for (BluetoothDevice bluetoothDevice : pairedDevices) {

                String pairedDevice = bluetoothDevice.getAddress();

                if (Objects.equals(pairedDevice, bleAddress)) {
                    digipenKit.connect(bluetoothDevice);
                    pencoderApplication.on_foundPairedDevice(bleAddress);
                    break;
                }
            }
        }
    }

    public void disconnectDigipen() {
        digipenKit.close();
        digipenKit = null;
        digipenKit = new DigipenKit(this, getApplicationContext(), DIGIPEN_MASK);
    }

    public void startDigipenCalibration() {
        digipenKit.digipenData.digipenCalibration.doCalibrate(true);
    }

    public void stopDigipenCalibration() {
        digipenKit.digipenData.digipenCalibration.doCalibrate(false);
    }

    public CalibrationData getCalibrationData() {
        return DigipenCalibration.calibrationData;
    }

    public void startDigipenStream() {
        if (digipenKit.digipenData.settings.sendSettingsTransaction(Settings.START_STREAM) == Settings.SettingsReturn.OK) {

            //penBehaviorChecker.startTimeOutChecker();
            //penBehaviorChecker.timeOutChecker.checkForStream("stream0", 1000);
            PEN_IS_STREAMING = true;
        }
    }

    public void stopDigipenStream() {
        if (digipenKit.digipenData.settings.sendSettingsTransaction(Settings.STOP_STREAM) == Settings.SettingsReturn.OK) {

            //penBehaviorChecker.stopTimeOutChecker();
            PEN_IS_STREAMING = false;
        }
    }

//    private void getSettings() {
//
//        File folder = new File(Environment.getExternalStorageDirectory() + "/Pencorder/", "Settings");
//        appSettings = new AppSettings();
//
//        try {
//
//            JSONObject file = Tools.loadJsonFile(folder, "settings.json");
//
//            JSONObject settings = file.getJSONObject("settings");
//            appSettings.leftHanded = settings.getJSONObject("handedness").getString("left_handed");
//            appSettings.rightHanded = settings.getJSONObject("handedness").getString("right_handed");
//
//            appSettings.genderMale = settings.getJSONObject("gender").getString("gender_male");
//            appSettings.genderFemale = settings.getJSONObject("gender").getString("gender_female");
//
//            appSettings.maxAgeInDays = settings.getJSONObject("calibration").getInt("max_age_in_days");
//
//            appSettings.criticalBatteryPower = settings.getJSONObject("battery").getInt("critical_battery_power");
//
//            appSettings.voice_language = settings.getJSONObject("voice").getString("language");
//            appSettings.voice_pitch = settings.getJSONObject("voice").getDouble("pitch");
//            appSettings.voice_rate = settings.getJSONObject("voice").getDouble("rate");
//
//            appSettings.bearOut_minTimeInAir = settings.getJSONObject("bearOut").getInt("min_time_in_air");
//            appSettings.bearOut_timeInAir = settings.getJSONObject("bearOut").getInt("time_in_air");
//            appSettings.bearOut_maxTimeInAir = settings.getJSONObject("bearOut").getInt("max_time_in_air");
//            appSettings.bearOut_minimumForceThreshold = settings.getJSONObject("bearOut").getInt("min_force_threshold");
//
//            boolean inPictures = settings.getJSONObject("representation").getBoolean("in_pictures");
//            //appSettings.setLabelType(inPictures, settings.getJSONObject("representation").getString("picture_folder"));
//
//        } catch (Exception e) {
//
//            pencoderApplication.on_initError("ERROR IN SETTINGS");
//        }
//    }

    private ArrayList<String> getLabels() {

        File folder = new File(Environment.getExternalStorageDirectory() + "/Pencorder/", "Labels");
        ArrayList<String> labels = new ArrayList<>();

        try {

            JSONObject file = Tools.loadJsonFile(folder, "labels.json");
            Iterator<String> keys = file.keys();

            String labelType = keys.next();

            switch (labelType) {

                case "labels":

                    JSONArray JsonLabels = file.getJSONArray(labelType);
                    for (int i = 0; i < JsonLabels.length(); i++) {

                        String label = JsonLabels.getString(i);
                        labels.add(label);
                    }

                    return labels;

                default:

                    pencoderApplication.on_initError("ERROR READING LABELS");
                    return null;
            }

        } catch (Exception e) {

            pencoderApplication.on_initError("ERROR INITIALIZING");
            return null;
        }
    }

//    LabelMachine initLabelMachine() {
//
//        if (labelMachine == null) {
//            return resetLabelMachine();
//        } else {
//            if (labelMachine.folder != null) {
//                return labelMachine;
//            } else {
//                return resetLabelMachine();
//            }
//
//        }
//    }
//
//    private LabelMachine resetLabelMachine() {
//        ArrayList<String> labels = getLabels();
//        return labelMachine = new LabelMachine(labels, sessionData.folder);
//    }

    public void getDigipenInformation() {
        onDigipenInformationChange(deviceInformation);
    }

    public void initSessionData() {
        sessionData = new SessionData();
        //labelMachine = resetLabelMachine();
    }

    public void initSessionFolder() {
        sessionData.folder.mkdirs();
        DigipenCalibrator.writeCalibrationFile(sessionData.folder, sessionData);
    }

    @Override
    public void onDigipenDataTransfer(StreamData digipenStreamData, Error error) {
        if (error == null) {
            digipenConnect.on_digipenDataStream(digipenStreamData);
            //penBehaviorChecker.check(digipenStreamData);
        }
    }

    @Override
    public void onDigipenCalibration(DigipenCalibration.CalibrationState calibrationState, Double stepProgress, DigipenCalibration.CalibrationAction calibrationAction) {
        digipenConnect.on_digipenCalibration(calibrationState, stepProgress, calibrationAction);
    }

    @Override
    public void onDigipenSettingsChange(int COMMAND, boolean success, String commandOutput) {

    }

    @Override
    public void onDigipenInformationChange(DeviceInformation deviceInformation) {

        this.deviceInformation = deviceInformation;

        if (digipenConnect != null) {

            // is g_connected
            if (deviceInformation.isConnected) {

                PEN_IS_CONNECTED = true;
                digipenConnect.on_digipenIsConnected(true);
                digipenConnect.on_digipenLastCalibrationDateChanged(DigipenCalibration.calibrationData.lastUpdateDate);

            } else {

                PEN_IS_CONNECTED = false;
                PEN_IS_STREAMING = false;
                digipenConnect.on_digipenIsConnected(false);
            }

            // digipenBattery changed
            if (DeviceInformation.batteryLevel >= 0) {
                digipenConnect.on_digipenBatteryHasChanged(DeviceInformation.batteryLevel);
            }

            // serial changed
            if (!Objects.equals(deviceInformation.serialNumber, "")) {
                digipenConnect.on_digipenSerialHasChanged(deviceInformation.serialNumber);
            }

            // name changed
            if (!Objects.equals(deviceInformation.bleName, "")) {
                digipenConnect.on_digipenNameHasChanged(deviceInformation.bleName);
            }

            // fwVersion changed
            if (!Objects.equals(deviceInformation.firmwareRevision, "")) {
                digipenConnect.on_digipenFirmwareVersionHasChanged(deviceInformation.firmwareRevision);
            }
        }
    }

    @Override
    public void onDigipenFirmwareUpdate(Settings.SettingsReturn status, int progress) {

    }

    @Override
    public void on_PenOffPaper(long timeInAir) {
        digipenConnect.on_digipenOffPaper();
    }

    @Override
    public void on_PenIsNotStreaming() {
        digipenConnect.on_digipenIsNotStreaming();
    }
}
