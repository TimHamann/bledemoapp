package com.example.tim.bledemoapp3.thiemo;

import com.stabilo.digipenkit.StreamData;

/**
 * Created by thiemodaubner on 08.03.18.
 * Cool
 */

public class PenBehaviorChecker implements TimeOutChecker.TimeOut {

    private BehaviorChecker behaviorChecker;

    private long start = 0;

    private double minForce;
    private int minimumTimeInAir;

    boolean thePenWasOnPaper = false;
    private int counter = 0;

    TimeOutChecker timeOutChecker;

    interface BehaviorChecker {
        void on_PenOffPaper(long timeInAir);
        void on_PenIsNotStreaming();
    }

    PenBehaviorChecker(BehaviorChecker behaviorChecker, double minForce, int minimumTimeInAir) {

        this.behaviorChecker = behaviorChecker;
        this.minForce = minForce;
        this.minimumTimeInAir = minimumTimeInAir;

        timeOutChecker = new TimeOutChecker(this);
    }

    void setMinimumTimeInAir(int time) {
        this.minimumTimeInAir = time;
    }

    void check(StreamData streamData) {

        double force = streamData.getForce().getValue();
        checkTimeInAir(force);

        timeOutChecker.itIsDone("stream" + counter);
        counter++;
        timeOutChecker.checkForStream("stream" + counter, 1000);
    }

    void stopTimeOutChecker() {
        timeOutChecker.clear();
    }

    void startTimeOutChecker() {
        counter = 0;
        timeOutChecker.isChecking = true;
    }

    private void checkTimeInAir(double force) {

        // Wenn Kraft innerhalb des thresholds
        if (force < minForce) {

            if (thePenWasOnPaper) {

                // Wenn das die erste Aufnahme in Luft ist
                if (start == 0) {

                    // Setze die aktuelle Zeit
                    start = System.currentTimeMillis();
                }

                // Berechne die Zeit in der Luft anhand des ersten Zeitpunkts in der Luft und jetzt.
                long timeInAir = System.currentTimeMillis() - start;

                if (timeInAir > minimumTimeInAir) {

                    behaviorChecker.on_PenOffPaper(timeInAir);
                    thePenWasOnPaper = false;
                }
            }

        } else if (force > minForce) {
            start = 0;
            thePenWasOnPaper = true;
        }
    }

    @Override
    public void on_timeOut() {
        behaviorChecker.on_PenIsNotStreaming();
    }
}
