package com.example.tim.bledemoapp3.thiemo;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.tim.bledemoapp3.R;

import java.util.HashSet;

public class DigipenFinder extends Activity implements BLEConnector.BLEResult {

    private static int REQUEST_ENABLE_BT = 1;
    private static int REQUEST_ENABLE_GPS = 2;
    private static final int REQUEST_LOCATION_PERMISSION = 3;
    private static final int REQUEST_FILE_WRITE_PERMISSION = 4;

    private LinearLayout mainLayout;
    BLEConnector bleConnector;
    private LocationManager locationManager;
    private BluetoothAdapter bluetoothAdapter;
    private ConnectionHolder connectionHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_digipen);

        // Reset the dataholder ble device
        connectionHolder = ((ConnectionHolder) this.getApplication());
        connectionHolder.setBleDevice(null);

        mainLayout = findViewById(R.id.mainLayout);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager != null ? bluetoothManager.getAdapter() : null;

        checkRequirement();
    }

    @Override
    protected void onResume() {

        super.onResume();

        try {
            bleConnector.scanForDevices(true);
        } catch (Exception e) {
            generatePermissionDeniedMessage();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_ENABLE_BT) {

            if (resultCode == Activity.RESULT_CANCELED) {

                generatePermissionDeniedMessage();

            } else {

                checkRequirement();
            }

        } else if (requestCode == REQUEST_ENABLE_GPS) {

            if (resultCode == Activity.RESULT_CANCELED) {

                generatePermissionDeniedMessage();

            } else {

                checkRequirement();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {

            case REQUEST_LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkRequirement();
                } else {
                    generatePermissionDeniedMessage();
                }
            }
            case REQUEST_FILE_WRITE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkRequirement();
                } else {
                    generatePermissionDeniedMessage();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

        try {
            bleConnector.scanForDevices(false);
        } catch (Exception e) {
            generatePermissionDeniedMessage();
        }
    }

    @Override
    public void onBLEScanResult(HashSet<BLEConnector.BLEDevice> bleDevices) {

        if (mainLayout != null) {
            mainLayout.removeAllViews();
        }

        for (final BLEConnector.BLEDevice bleDevice : bleDevices) {

            final Button deviceButton = new Button(getApplicationContext());
            int buttonId = View.generateViewId();
            deviceButton.setId(buttonId);
            String name = bleDevice.name;

            if (name == null) {
                name = "NO DEVICE NAME";
            }

            deviceButton.setText(getString(R.string.device_name, name, bleDevice.rssi, bleDevice.address.toString()));

            // ---- Set the onClickListener to connect to the device when click
            deviceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        Intent startActivity = new Intent();
                        connectionHolder.setBleDevice(bleDevice);
                        bleConnector.scanForDevices(false);
                        setResult(RESULT_OK, startActivity);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            try {
                mainLayout.addView(deviceButton);
            } catch (Exception ignored) {
            }
        }

    }

    /**
     * This method is used to check if 2 requirements are fulfilled.
     * <p>First you need to check if bluetooth is enabled</p>
     * <p>Second you have do check if the location services (most GPS) are enabled</p>
     * <p>If this requirements are fufilled a new bleConnector will be created and the scan for devices
     * starts</p>
     */
    private void checkRequirement() {

        // Check for permission for access fine location
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);

        } else {

            // If this is ok check for permissions for writing in storage
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_FILE_WRITE_PERMISSION);

            } else {

                // If this is ok check if bluetooth is enabled
                if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {

                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

                } else {

                    // If this is ok, check if location services are enabled
                    locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

                    if (locationManager == null || !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                        Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(gpsOptionsIntent);

                    } else {


                        // If this is ok (yehaa) create a BLEConnector object
                        try {

                            bleConnector = new BLEConnector(this, bluetoothAdapter);
                            bleConnector.setSearchFilterOn(true);
                            bleConnector.setScanPeriod(10 * 60 * 1000);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private void generatePermissionDeniedMessage() {

        Toast.makeText(getApplicationContext(), "PERMISSION DENIED", Toast.LENGTH_LONG).show();
        finish();

    }
}
